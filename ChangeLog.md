# U2C V2.1 Change Log

* Move one key program circuit to user terminal from pc terminal;
* LED of rxd and txd light with ft232 c0/c1;
* Add crystal oscillator of ft232;

# U2C V2.0 Change Log

* Use FT232 of PL2303;
* Isolator digital and power.